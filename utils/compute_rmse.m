function [Err, relErr, rmse_tr, rmse_te] = compute_rmse(U, tcinfo)
% Compute the errors by using the function mex/tspmaskmult. 
% INPUT: 
% 1. U: cell array containing the factors [U1, U2, U3].  
% 2. tcinfo: struct containing tensor completion-related information. See
% utils/build_tcinfo_frSpT.m 

  comp_RMSE = @(Delta) sqrt(mean(Delta.^2));
  ntr     = tcinfo.sz_tr ;
  nte     = tcinfo.sz_te ;

  err_tr = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K)... 
           - tcinfo.Ttr;
  err_te = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.Ite, tcinfo.Jte, tcinfo.Kte)...
           - tcinfo.Tte; 

  rmse_tr = comp_RMSE(err_tr);
  rmse_te = comp_RMSE(err_te);

  % rmse_all     = sqrt(  (rmse_tr^2 *ntr + rmse_te^2 *nte)/(ntr+nte) ); 
  
  Err = sqrt(rmse_tr^2 *ntr + rmse_te^2 *nte);
  relErr = Err / sqrt(sum(tcinfo.Ttr.^2) + sum(tcinfo.Tte.^2)); 

end

