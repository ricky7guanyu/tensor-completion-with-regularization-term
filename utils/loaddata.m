function output = loaddata(name, varargin)

if nargin < 1 
    name = 'ml100k';
end

switch name
    case 'fia'
        output = load_fia(); 
    otherwise
        error(sprintf('The data set %s is not available\n', name));
end




function output = load_fia()
% FIA data from Nørgaard & Ridder, Chemometrics & 
% Intelligent Laboratory Systems, 1994, 23, 107-114
% 
% X contains the 12 x 100 x 89 spectral FIA data in 
% a 12 x 8900 matrix. To convert to a three-way array 
% in MATLAB 5 and higher type <X = reshape(X,DimX)>.
% The concentrations of the three analytes are provided
% in y
% 
% Xs is a set of three pure samples averaged from 
% triplicate measurements. ys holds the concentrations
% 
% DimX gives the dimensions of the three-way array
% 
% For further info see above reference or
% http://www.models.kvl.dk/users/rasmus/

% Flow Injection. As a real benchmark dataset with
% auxiliary information, we used the ‘Rank-deficient spectral FIA dataset’2, which
% consists of results of flow injection analysis on 12 different chemical substances.
% They are represented as a tensor of size 12 (substances)×100 (wavelengths)×89
% (reaction times).
% We constructed three similarity matrices for the three modes as follows. Since
% 12 chemical substances differ in contents of three structural isomers of a certain
% chemical compound, each substance can be represented as a three-dimensional
% feature vector. We defined the similarity between two substances as the inverse
% of Euclidean distance between their feature vectors. Also, since wavelength and
% reaction time have continuous real values, we simply set the similarity of two
% consecutive wavelength values (or reaction time values) to one.

% Feature matrix of the 12 substances: y. S(phi_i, phi_j) = 1/|phi_i-phi_j|_2 
% W of wavelengths, time: a simple chain graph. 
    output = load('datasets/fia/fia.mat'); 
    output.feat_r = output.y; 
    output.name = 'fia'; 
end

end
