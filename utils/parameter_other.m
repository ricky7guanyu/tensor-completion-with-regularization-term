function [para_TNCP,para_TFAI,para_AirCP] = parameter_other(T_data,opts,para)


if isfield(opts, 'n_hpconfigs')
    n_hpconfigs = opts.n_hpconfigs;
else
    n_hpconfigs = 10;
end


    alpha_TNCP = para.alpha_TNCP_min + (para.alpha_TNCP_max-para.alpha_TNCP_min).*rand(n_hpconfigs,1);
    alpha_TNCP = 10.^alpha_TNCP;
    beta_TNCP = para.beta_TNCP_min + (para.beta_TNCP_max-para.beta_TNCP_min).*rand(n_hpconfigs,1);
    beta_TNCP = 10.^beta_TNCP;
    
    
     alpha_TFAI = para.alpha_TFAI_min + (para.alpha_TFAI_max-para.alpha_TFAI_min).*rand(n_hpconfigs,1);
     alpha_TFAI = 10.^alpha_TFAI;
%    
%     
     alpha_AirCP = para.alpha_AirCP_min + (para.alpha_AirCP_max-para.alpha_AirCP_min).*rand(n_hpconfigs,1);
     alpha_AirCP = 10.^alpha_AirCP;
     beta_AirCP = para.beta_AirCP_min + (para.beta_AirCP_max-para.beta_AirCP_min).*rand(n_hpconfigs,1);
     beta_AirCP = 10.^beta_AirCP;
%     
    
    for i = 1:n_hpconfigs
       opts.alpha_TNCP(1) = alpha_TNCP(i);
        opts.alpha_TNCP(2) = beta_TNCP(i);
        opts.alpha_TNCP(3) = beta_TNCP(i);
        
       opts.alpha_TFAI = alpha_TFAI(i);
        
        opts.alpha_AirCP(1) = alpha_AirCP(i);
        opts.alpha_AirCP(2) = beta_AirCP(i);
        
        tcinfo =  build_tcinfo_frSpT(T_data, opts.SR);
         
        for multitest = 1:7
            t0 = tic;
        opts.U = initial_U_testtensor(tcinfo, opts);
        opts.tinit =toc(t0);
           score.Out_TNCP = tc_TNCP(tcinfo, opts);
           score.TNCP(multitest,i) = score.Out_TNCP.test_RMSE(end);
            
             score.Out_TFAI = tc_TFAI(tcinfo, opts);
             score.TFAI(multitest,i) = score.Out_TFAI.test_RMSE(end);
%             
             score.Out_AirCP = tc_AirCP(tcinfo, opts);
             score.AirCP(multitest,i) = score.Out_AirCP.test_RMSE(end);
        end
    end
        
      
   [value1,index1] = min(mean(score.TNCP));
     [value2,index2] = min(mean(score.TFAI));
     [value3,index3] = min(mean(score.AirCP));
   para_TNCP = [alpha_TNCP(index1),beta_TNCP(index1),beta_TNCP(index1),value1];
     para_TFAI = [alpha_TFAI(index2),value2];
     para_AirCP = [alpha_AirCP(index3),beta_AirCP(index3),value3];

   
   
end