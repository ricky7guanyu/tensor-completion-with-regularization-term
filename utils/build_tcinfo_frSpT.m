function tcinfo = build_tcinfo_frSpT(T, SR)
% This function creates a standard struct tcinfo, which is composed of
% I, J, K, Ttr, % coo format
% Ite, Jte, Kte, Tte.
% 
if nargin < 2
    SR = .8; 
end
dims = size(T); 

[c1,c2] = find(T);
nz = size(c1,1);
N = ceil(SR*nz);

s_tr = randsample(nz, N);
s_tr = sort(s_tr);
s_te = setdiff(1:nz, s_tr);

I = c1(s_tr,1);
J = c1(s_tr,2);
K = c1(s_tr,3);

Ite = c1(s_te,1);
Jte = c1(s_te,2);
Kte = c1(s_te,3);

Ttr = c2(s_tr); 
Tte = c2(s_te); 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
trans = [];
Omega = [I,J,K];
for i = 1:3
    bte = setdiff(1:3,i);
    Omega_temp = Omega(:,[bte,i]);
   for j = 1:N
       trans(j,i) = Omega_temp(j,1) + (Omega_temp(j,2)-1)*dims(bte(1));
   end
end
Omega_mat{1} = [trans(:,1),I]; 
Omega_mat{2} = [trans(:,2),J];
Omega_mat{3} = [trans(:,3),K]; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tcinfo = struct('I', uint32(I), 'J', uint32(J), 'K', uint32(K),...
                'Ite', uint32(Ite), 'Jte', uint32(Jte), 'Kte', uint32(Kte),...    
                'Ttr', Ttr, 'Tte', Tte,...
                'sz_tr',numel(I),'sz_te',numel(Ite),...
                'sz_tensor', dims, ... 
                'Omega_mat', {Omega_mat}); 

end
