function U = altmincg_initialize(tcinfo, opts, method_init)
if nargin < 3
    method_init = 'random';
end
m = tcinfo.sz_tensor; 
k = numel(m); 
R = opts.R; 
if isfield(opts,'U')
    U = opts.U;
    return; 
end
switch method_init
case 'spec'
   Z = sptensor([double(tcinfo.I),double(tcinfo.J),double(tcinfo.K)], tcinfo.Ttr, m);
   Zmat = reshape(Z, [m(1), m(2)*m(3)]); 
 
   [U1, S, U2] = svds(Zmat, R);
   % Zmat_hat = U1 * S * U2'; 
   % todo: how to get (U_{i})_{i=1,..,k}) from Zmat or (U1, S, U2) ? 

case 'random'
    U = {};
    for i = 1:k
       U{i} = randn(m(i),R); % randomly generate each factor
    end
end

end

