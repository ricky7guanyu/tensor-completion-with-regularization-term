function [W_sp, eps, sigma] = buildW_epsNN_GaussianKer(featMat, opts)
% Build an epsion-NN graph from features
    sparsity = opts.sparsity;
    Z = pdist2(featMat, featMat);
    sigma = var(Z(:))/5;
    [W_sp, eps] = buildW_sparsify(exp(-Z.^2/sigma), sparsity);
end

