function Lmat = computeLmat_fromW(W, opts)
    d = sum(W,1);
    if isfield(opts, 'type_graphLaplacian') && strcmp(opts.type_graphLaplacian,'normalized')
        Lmat = sparse(eye(size(W,1)) - diag(sqrt(d))*W*diag(sqrt(d)));
    else
        Lmat = sparse(diag(d)) - W;
    end
end

