function [para1,para2] = parameter1(T_data,opts,para)

% This function compute the lambda using grid search.
% Lambda(1) is the coeffient of U^{1} and krh(U^{3},U^{2})
% Lambda(2) is the coeffient of U^{2} and krh(U^{3},U^{1})
% Lambda(3) is the coeffient of U^{3} and krh(U^{2},U^{1})
% Lambda(4) is the coeffient of Laplacian term

    n_hpconfigs = 10;

   
    beta = para.beta_min + (para.beta_max-para.beta_min).*rand(n_hpconfigs,1);
    beta = 10.^beta;
   opts.Lambda(1) = 0.0111;
        opts.Lambda(2) = 0.0016;
        opts.Lambda(3) = 0.0016;
    for i = 1:n_hpconfigs
       
        opts.Lambda(4) = beta(i);
        
        tcinfo{i} =  build_tcinfo_frSpT(T_data, opts.SR);
        
        score.Out_CG{i} = tc_altmin_cg_old(T_data, tcinfo{i}, opts);
        score.Out_ADMM{i} = tc_altmin_ADMM_old(T_data, tcinfo{i}, opts);
        score.CG(i) = score.Out_CG{i}.test_RMSE(end);
        score.ADMM(i) = score.Out_ADMM{i}.test_RMSE(end);
    end
    [value1,index1] = min(score.CG);
    [value2,index2] = min(score.ADMM);
    para1 = [beta(index1),value1];
    para2 = [beta(index2),value2];
    
    
    
end






