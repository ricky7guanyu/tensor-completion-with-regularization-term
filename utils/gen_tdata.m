function output = gen_tdata(T, params_data)
% INPUT:
% T: an input tensor. 
% params_data: a struct storeing parameters needed for this function. Precisely, 
% - gdatNoise: 1d array of size 2. 
%   - gdatNoise(1) = 0 or 1, to indicate whether the noise is to be
%   added to the input tensor.  
%   - gdatNoise(2) stores the value of the noise level, in terms of
%   standard variation (sigma < 1) of the Gaussian noise model or
%   the SNR (assumed to be >1). The SNR measurement is recommended since 
%   it is a ratio and is a rather standard way for measuring the
%   noise level. 
%
% OUTPUT: 
% output: a struct storing 
% - T: the tensor after the additive noise (possibly zero),
% - T_clean: equals the original tensor if the additive noise is non-zero. 
% 
% See GRMC-core/Tester.m/gen_gdata (link
% here https://gitlab.com/shuyudong.x11/grmc-core/blob/master/@Tester/Tester.m), for details. 
% Contact: shuyu.dong@uclouvain.be. 
% October, 2019. 

if nargin < 2
    params_data = struct('gdatNoise', [0, 20]); 
end

N = numel(T(:)); 
if params_data.gdatNoise(2) > 1 
    % This is the case where the noise parameter is in
    % SNR(dB).
    SNRdB = params_data.gdatNoise(2);
    noise_sigma = norm(T(:))/sqrt(prod(size(T)))/10^(SNRdB/20);
else
    % This is the case where the noise parameter is the standard devaiation.
    noise_sigma = params_data.gdatNoise(2);
end

if params_data.gdatNoise(1)
    T_clean = T;
else
    T_clean = nan;
end

T2 = T(:) + params_data.gdatNoise(1)*...
		noise_sigma*...
		randn(N, 1);

output.T = reshape(T2, size(T)); 
output.T_clean = T_clean; 

end

