function [L, W] = buildLmat_fia(data)

% Computes a 12x12 pairwise distance matrix
% then computes the elementwise inverse. 

% 1. compute W(i,j) = 1/|phi_i - phi_j|_2. 
% 2. L = diag(diag(W*ones)) - W; 

feat_r = data.feat_r;
n = size(feat_r,1);

pdists = pdist2(data.feat_r, data.feat_r); 

W = 1./pdists; 
W(isinf(W)) = 0; 

L = diag(sum(W,1)) - W; 


end
