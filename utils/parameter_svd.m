

clear all
startup
load Experiment1_data.mat

%% set the stopping criteria

opts.maxtime = 10;
opts.tol = 10^-12;
opts.maxit = 500;

%% set the range of parameters

para.alpha_min = -7;
para.alpha_max = -2;
para.beta_min = -5;
para.beta_max = -1;
%
opts.SR = 0.003;


%% test three models

opts.method_init = 'HOSVD';

n_hpconfigs = 50;
alpha = para.alpha_min + (para.alpha_max-para.alpha_min).*rand(n_hpconfigs,1);
alpha = 10.^alpha;
beta = para.beta_min + (para.beta_max-para.beta_min).*rand(n_hpconfigs,1);
beta = 10.^beta;
for i = 1:n_hpconfigs
    opts.Lambda(1) = alpha(i);
    opts.Lambda(2) = alpha(i);
    opts.Lambda(3) = alpha(i);
    opts.Lambda(4) = beta(i);
    
    tcinfo =  build_tcinfo_frSpT(T_data, opts.SR);
    
    
    t0 = tic;
    opts.U = initial_U_testtensor(tcinfo, opts);
    opts.tinit = toc(t0);
    score.Out_CG = tc_altmin_cg(tcinfo, opts);
    score.CG(i) = score.Out_CG.test_RMSE(end);
    score.Out_ADMM = tc_altmin_ADMM(tcinfo, opts);
    score.ADMM(i) = score.Out_ADMM.test_RMSE(end);
    
    
    
end
[value1,index1] = min(score.CG);
[value2,index2] = min(score.ADMM);
para1 = [alpha(index1),alpha(index1),alpha(index1),beta(index1),value1];
para2 = [alpha(index2),alpha(index2),alpha(index2),beta(index2),value2];
%




%
if ismac
    % Code to run on Mac platformr
    path_results = '~/tensorcomp_reg/results';
elseif isunix
    % Code to run on Linux platform
    path_results = '~/tensorcomp_reg/results';
elseif ispc
    % Code to run on Windows platform
    path_results = 'C:\Users\yguan\tensorcomp_reg\results';
else
    disp('Platform not supported')
end
save(sprintf('%s/Experiment1_0.003svd_1104.mat', path_results));