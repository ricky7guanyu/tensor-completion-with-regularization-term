function [T_omega,Omega,W,k,N,U1,G2] = construct_testtensor(construct,m,samplerate,R)





switch construct
    case 1,
        %randomly generated T 
        
        
         W1 = randn(m(1),R); 
         W2 = randn(m(2),R);
         W3 = randn(m(3),R);
         Z1 = W1*(khatrirao(W3,W2))';
%         Z{2} = W2*(khatrirao(W3,W1))';
%         Z{3} = W3*(khatrirao(W2,W1))';
          
        
       
        k = length(m); 
        
         if k > 2
            size1 = m(1)*m(2)*m(3);
            for order = 4:k
                size1 = size1*m(order);
            end
         end
        
        p = 2;
        
        N = samplerate*size1;
        
       % G1 = gsp_graph(Z1);
        
        G1 = gsp_community(m(1));

       %[G] = gsp_hypergraph(m(1),W1);
        
%         for i = 1:k
%             G{i} = reverse_eig(LAMBDA{i});
%             A{i} = V{i}*G{i};
%             M{i} = A{i}*Z{i}; 
%         end
        G2 = double(G1.L);
       % [V,LAMBDA,~] = eig(full(G2));
       [V,LAMBDA,~] = svd(full(G2));
       % G = reverse_eig(LAMBDA);
        LEN = length(LAMBDA);
       for i = 1:LEN-1
           G(i,i) = LAMBDA(i,i)^(-p);
       end
       G(LEN,LEN) = 0;
         
      
        A = V*G; 
        
        M = A*Z1;
        W = reshape(M,m);
        
      
     
        
        row = randsample(size1,N);
              
       
            [I1,I2,I3] = ind2sub(size(W),row);
            Omega = [I1,I2,I3];
     
        
    
        
        
        T_omega = zeros(m);
        
        for j = 1:N
            if k == 3
               T_omega(Omega(j,1),Omega(j,2),Omega(j,3)) = W(Omega(j,1),Omega(j,2),Omega(j,3));
            end
            
        end
        
        
       U1 = {};
      
        
        

        

    case 2,
        
        % CASE 2 is totally T = T_omega and U{i} is exactlly generated to T
        
        k = length(m);    
        for i = 1:k
            U1{i} = randn(m(i),R);
        end
        
        
       
        if k > 2
           temp = khatrirao(U1{3},U1{2});
           for order = 4:k
               temp = khatrirao(U1{order},temp);
           end
        elseif k == 2
           temp = U1{2};
        end
           
        W = U1{1}*temp';
        W = reshape(W,m);
        
  
        
       N = m(1)*m(2)*m(3);
       
      [I1,I2,I3] = ind2sub(size(W),(1:N)');
      Omega = [I1,I2,I3];
       
      
        
       %Omega is stored for every index in T
        T_omega = W;
        
        
   case 3,
        
        
        k = length(m);    
        for i = 1:k
            U1{i} = randn(m(i),R);
        end
        
        
       
        if k > 2
           temp = khatrirao(U1{3},U1{2});
           for order = 4:k
               temp = khatrirao(U1{order},temp);
           end
        elseif k == 2
           temp = U1{2};
        end
           
        W = U1{1}*temp';
        W = reshape(W,m);
        
     
        if k > 2
            size1 = m(1)*m(2)*m(3);
            for order = 4:k
                size1 = size1*m(order);
            end
        end
        
     
        
        N = samplerate*size1;
        % N is randonmsampling from all the elements in T
        
        row = randsample(size1,N);
              
       
            [I1,I2,I3] = ind2sub(size(W),row);
            Omega = [I1,I2,I3];
        
        
        % T_omega is used for synthetic example, but for real data, the
        % given data is actually T-omega
        
        
        
        T_omega = zeros(m);
        
        for j = 1:N
            if k == 3
               T_omega(Omega(j,1),Omega(j,2),Omega(j,3)) = W(Omega(j,1),Omega(j,2),Omega(j,3));
            end
            
        end
        

        
        
        G2 = 0;
               
    case 4,
        
        
       k = length(m);
        
        for i = 1:k
            U1{i} = randn(m(i),R);
        end
        
        
       
        if k > 2
           temp = khatrirao(U1{3},U1{2});
           for order = 4:k
               temp = khatrirao(U1{order},temp);
           end
        elseif k == 2
           temp = U1{2};
        end
           
        W = U1{1}*temp';
        W = reshape(W,m);
        
        
        
         if k > 2
            size1 = m(1)*m(2)*m(3);
            for order = 4:k
                size1 = size1*m(order);
            end
        end
        
     
        
        N = samplerate*size1;
        % N is randonmsampling from all the elements in T
        
        row = randsample(size1,N);
              
       
            [I1,I2,I3] = ind2sub(size(W),row);
            Omega = [I1,I2,I3];
        
        
        
        error_tensor = randn(m);
        
        W = W + 10^-4*error_tensor;
        
        % T_omega is used for synthetic example, but for real data, the
        % given data is actually T-omega
        
        
        
        T_omega = zeros(m);
        
        for j = 1:N
            if k == 3
               T_omega(Omega(j,1),Omega(j,2),Omega(j,3)) = W(Omega(j,1),Omega(j,2),Omega(j,3));
            end
               
        end
        
    case 5
        
        k = length(m); 
        
       for i = 1:k
            U1{i} = randn(m(i),R);
        end
        
        
       
        if k > 2
           temp = khatrirao(U1{3},U1{2});
           for order = 4:k
               temp = khatrirao(U1{order},temp);
           end
        elseif k == 2
           temp = U1{2};
        end
           
        W = U1{1}*temp';
        W = reshape(W,m);
     
       
        error_tensor = randn(m);
        
        W = W + 10^-4*error_tensor;
        
    
         
       N = m(1)*m(2)*m(3);
       
      [I1,I2,I3] = ind2sub(size(W),(1:N)');
      Omega = [I1,I2,I3];
        
       %Omega is stored for every index in T
        T_omega = W;
        
   case 6,
        %randomly generated T 
        W = randn(m);    
        k = length(m);            
        if k > 2
            size1 = m(1)*m(2)*m(3);
            for order = 4:k
                size1 = size1*m(order);
            end
        end
        
     
        
        N = samplerate*size1;
        % N is randonmsampling from all the elements in T
        
        row = randsample(size1,N);
              
       
            [I1,I2,I3] = ind2sub(size(W),row);
            Omega = [I1,I2,I3];
     
        
    
        
        
        % T_omega is used for synthetic example, but for real data, the
        % given data is actually T-omega
        
        
        
        T_omega = zeros(m);
        
        for j = 1:N
            if k == 3
               T_omega(Omega(j,1),Omega(j,2),Omega(j,3)) = W(Omega(j,1),Omega(j,2),Omega(j,3));
            end
            
        end
        
        
        U1 = {};
        
        
        
        
     
end