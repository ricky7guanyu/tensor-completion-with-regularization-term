function save2mat(id_expe)

if nargin < 1 
	id_expe = 'experiment_x';
end

if ismac
    % Code to run on Mac platformr
    path_results = '~/tensorcomp_reg/results';
elseif isunix
    % Code to run on Linux platform
    path_results = '~/tensorcomp_reg/results';
elseif ispc
    % Code to run on Windows platform
    path_results = 'C:\Users\yguan\tensorcomp_reg\results';
else
    disp('Platform not supported')
end

%%% to add date identifier 
tmps = datetickstr(now,'hhMM_ddmmm_');
timeStr = tmps{1};

save(sprintf('%s/%s_%s.mat', path_results, id_expe, timeStr));

end