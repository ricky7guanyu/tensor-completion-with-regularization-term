function [L, t] = feed_Lmat(data, params, varargin)
% function feed_Lmat(self, mcinfo, varargin)
% This function feeds the graph Laplacian matrix to GRLRMC.L by constructing the matrix from data that is 
% only available on Omega. See [Tester.m/feed_Lmat in
% here](https://gitlab.com/shuyudong.x11/grmc-core/blob/master/@Tester/Tester.m). 
    
    if isfield(data, 'SIDEINFO') 
        % note: This is the idea of constructing graph Laplacian
        %       matrices from "side information" [Rao et al. 2015].
        %       Attention: "SIDEINFO" is *not* available for datasets
        %       currently available to us. Collecting SIDEINFO can be
        %       a time consuming data science task, python packages
        %       such as pandas, spark are often used (e.g. NYC taxi
        %       data) to extract datasets online and then processing
        %       the dataset properties to build side information. No
        %       precise knowledge available yet. This procedure is
        %       *not* available in the code 
        %       [exp-grmf-nips15](https://github.com/rofuyu/exp-grmf-nips15)
        %       by [Rao et al. 2015] but only described in the paper.
        %       => Hence this case is not active for now.
        feat_r = data.SIDEINFO.feat_r;
        feat_c = data.SIDEINFO.feat_c;
    else 
        switch data.name 
            case {'ml100k','sparsedata'}
            % This is the case with MovieLens Datasets. Assume that 
            % (1) data.coo is an array of size nnz x 3 such that
            %     matrix(data.coo(k,1), data.coo(k,2)) = data.coo(k,3);
            % (2) data.dims is an array of size 1 x 3 that contains the 
            %     sizes of the tensor along the three dimensions (user, movie,
            %     time).  
                hatM0 = sparse(double(data.coo(:,1)),...
                               double(data.coo(:,2)),...
                               double(data.coo(:,3)),...
                               data.dims(1),...
                               data.dims(2)); 
                [U,S,V] = svds(hatM0, params.hatM0_rank); 
                feat_r = U*S;
                feat_c = V*S;
                SR = numel(data.coo(:,1))/(data.dims(1)*data.dims(2)); 
            case {'traffic', 'fulldata'}
            % This is the case with the Traffic Dataset. Assume
            % data.mat is the data matrix of traffic. 
                feat_r = data.mat; 
                feat_c = data.mat';
                SR = 1; 
            case {'fia' }
                feat_r = data.feat_r; 
                feat_c = 0; 
                SR = 1; 
                L = struct('Lr', buildLmat_fia(data)); 
        end
    end
    if ~exist('L','var')
        [L, t] = buildLmat_fromFeat(struct('feat_r', feat_r, ...
                                       'feat_c', feat_c,...  
                                       'feat_samplrate', SR), ...
                                     params);
     end
end

