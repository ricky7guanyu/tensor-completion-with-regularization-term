function [A, I, r] = construct_tensor(example)
switch example.type,
    case 1,
        % later: maybe not all enties of A are required, since A is supersymmetric
        A = zeros(example.I,example.I,example.I);
        for i = 1:example.I
            for j = i:example.I
                for k = j:example.I
                    A(i,j,k) = randn; A(i,k,j) = A(i,j,k); A(j,i,k) = A(i,j,k);
                    A(j,k,i) = A(i,j,k); A(k,i,j) = A(i,j,k); A(k,j,i) = A(i,j,k);
                end
            end
        end
        I = example.I;
        r = example.r;
        
    case 2,
        % rr = r-1
        rr = example.r;
        A = zeros(example.I,example.I,example.I);
        for i = 1:rr
            for j = i:rr
                for k = j:rr
                    C(i,j,k) = randn; C(i,k,j) = C(i,j,k); C(j,i,k) = C(i,j,k);
                    C(j,k,i) = C(i,j,k); C(k,i,j) = C(i,j,k); C(k,j,i) = C(i,j,k);
                end
            end
        end
        for i = 1:example.I
            for j = i:example.I
                for k = j:example.I
                    E(i,j,k) = randn; E(i,k,j) = E(i,j,k); E(j,i,k) = E(i,j,k);
                    E(j,k,i) = E(i,j,k); E(k,i,j) = E(i,j,k); E(k,j,i) = E(i,j,k);
                end
            end
        end
        U_temp = qf(randn(example.I,rr));
        AA = TxM(TxM(TxM(C,1,U_temp),2,U_temp),3,U_temp);
        A = AA/norm(AA(:)) + 0.1*E/norm(E(:));
        I = example.I;
        r = example.r;
               
    case 3,
        % Partial symmetric example for our paper so application of Sym Jacobi
        vec1 = [-0.6060; 0.3195; 0.72885];
        vec2 = [0.7955; 0.2491; 0.5524];
        vec3 = [-0.0050; 0.9143; -0.4051];
        A = zeros(3,3,3);
        for i = 1:3
            A(:,:,i) = vec3(i) * vec1 * vec2' + vec1(i) * vec2 * vec3' + vec2(i) * vec3 * vec1';
        end
        I = 3;
        r = 1;       
        
    case 4, % Symmetric example for our paper rank1
        A(:, :, 1) =[...
            0.1182, 0.3220, 1.3986; ...
            0.3220, 1.2843, -0.8889; ...
            1.3986, -0.8889, -0.8642];
        A(:, :, 2) = ...
            [0.3220, 1.2843, -0.8889; ...
            1.2843, -0.2796, -1.4854; ...
            -0.8889, -1.4854, 0.2368];
        A(:, :, 3) = ...
            [1.3986, -0.8889, -0.8642; ...
            -0.8889, -1.4854, 0.2368; ...
            -0.8642, 0.2368, 0.7504];
        I = 3;
        r = 1;
        
    case 5, % Symmetric example for our paper rank222
        A(:, :, 1) =[...
            1.2753,   -0.5811,   -0.0725; ...
            -0.5811,  -0.8475,    0.0379; ...
            -0.0725,   0.0379,   -1.0573];
        A(:, :, 2) = [...
            -0.5811,  -0.8475,    0.0379; ...
            -0.8475,  -1.0771,   -0.6544; ...
            0.0379,   -0.6544,   -0.7375];
        A(:, :, 3) = [...
            -0.0725,   0.0379,   -1.0573; ...
            0.0379,   -0.6544,   -0.7375; ...
            -1.0573,  -0.7375,    0.1491];
        I = 3;
        r = 2;
        
    case 6, % potential example for early stopping argument
        A(:, :, 1) =[...
            0.1527,   -1.2859,    0.6235; ...
           -1.2859,    2.0300,   -0.4564; ...
            0.6235,   -0.4564,   -0.7072];
        A(:, :, 2) = [...
            -1.2859,   2.0300,   -0.4564; ...
             2.0300,   0.5067,   -0.4924; ...
            -0.4564,  -0.4924,   -1.4257];
        A(:, :, 3) = [...
             0.6235,  -0.4564,   -0.7072; ...
            -0.4564,  -0.4924,   -1.4257; ...
            -0.7072,  -1.4257,   -0.8216];
        I = 3;
        r = 1; % 2?
        
    case 7, % 7: example from Regalia's paper, "monotonically convergent algorithms
        % for symmteric tensor approximations", p16, example 4
        A = zeros(2,2,2);
        A(1,1,1) = -2.23;
        A(1,1,2) = 0.38; A(1,2,1) = A(1,1,2); A(2,1,1) = A(1,1,2);
        A(1,2,2) = -1.48; A(2,1,2) = A(1,2,2); A(2,2,1) = A(1,2,2);
        A(2,2,2) = -27.5;
        I = 2;
        r = 1;
end