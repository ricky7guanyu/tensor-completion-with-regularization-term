function [T_omega,Omega,N] = construct_testtensor1(m,samplerate,W,k)
            
            
          
            
            if k > 2
            size1 = m(1)*m(2)*m(3);
               for order = 4:k
                size1 = size1*m(order);
               end
            end
            
            
            N = ceil(samplerate*size1);
          row = randsample(size1,N);
              row = sort(row);
       
            [I1,I2,I3] = ind2sub(size(W),row);
            Omega = [I1,I2,I3];
     
          
         
           T_omega = zeros(m);
          
        for j = 1:N
            if k == 3
               T_omega(Omega(j,1),Omega(j,2),Omega(j,3)) = W(Omega(j,1),Omega(j,2),Omega(j,3));
            end
            
        end
        
       
            
end
            
       
        