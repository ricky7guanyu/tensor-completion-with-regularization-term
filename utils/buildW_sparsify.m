function [Wsp, eps] = buildW_sparsify(W, sparsity)
    W = W - diag(diag(W));

    % Compute the empirical cdf function 
    [cdf, w] = ecdf(W(:));

    % Find eps = ecdf^{-1}(1-sparsity) 
    tol = 1e-7; ids = [];
    while isempty(ids) 
        tol = 10*tol;
        ids = find( abs(cdf - (1-sparsity))<tol );
    end
    % Take the value in the middle of the targeted bin:
    len = numel(ids);
    eps = w(ids(ceil(len/2)));

    % Return the sparse graph adjacency matrix
    Wsp = sparse(W.*(W>=eps));
end

