function [L, t] = buildLmat_fromFeat(feat, opts, varargin)
    
    fprintf('\n.....Start building Lmat from data features with method="%s"... ', opts.methodname); 
    t0=tic;
    switch opts.methodname 
        case 'epsNN-GaussianKernel'
            [Wr , eps_r, sigma]= buildW_epsNN_GaussianKer(feat.feat_r, opts);
            Wc = sparse(size(feat.feat_c,1), size(feat.feat_c,1));
            opts.eps_r = eps_r;
            opts.sigma_r = sigma;
        case 'kNN-Euclidean'
            % example: use `pdist2(_,_, 'euclidean', 'Smallest', k)`, c.f. [here](https://nl.mathworks.com/help/stats/pdist2.html).
            error('The method %s for building graph Laplacian matrices is not available yet.\n', opts.methodname);
        case 'kNN-GaussianKernel'
            % example: use `pdist2(_,_, 'euclidean', 'Smallest', k)`, c.f. [here](https://nl.mathworks.com/help/stats/pdist2.html).
            error('The method %s for building graph Laplacian matrices is not available yet.\n', opts.methodname);
        otherwise
            error('The method %s for building graph Laplacian matrices is not available.\n', opts.methodname);
    end
    t = toc(t0);
    fprintf('Done: '); toc(t0);
    L = struct('Lr', computeLmat_fromW(Wr, opts), ...
               'Lc', computeLmat_fromW(Wc, opts),...
               'Wr', Wr, ...
               'feat_samplrate', feat.feat_samplrate,...
               'opts_buildLmat', opts); 
    

end


