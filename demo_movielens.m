%% Demo on the MovieLens 100k dataset 
clear all
startup

% Load the movielens data and generate similar matrices
opts.m = [943 1682 7];
data1 = load('u.data');
subs = [data1(:,1),data1(:,2),weekday(data1(:,4))];
vals = data1(:,3);
% Dataset in tensor format 
T_data = sptensor(subs,vals,opts.m);
% Sampling rate (among entries given in the dataset)
opts.SR = 0.8;


% Build graph information
data = struct('name', 'ml100k', 'coo',[data1(:,1),data1(:,2), data1(:,3)], 'dims', opts.m(1:2));
Lmat_params = struct('methodname','epsNN-GaussianKernel', 'hatM0_rank', 20, ...
    'sparsity', 0.005);
graph = feed_Lmat(data, Lmat_params);
 

% Algorithm's parameters
opts.R = 10;         % maximal rank of the CPD model 
opts.L{1} = graph.Lr;
opts.L{2} = graph.Lc;
opts.L{3} = zeros(opts.m(3),opts.m(3));


opts.maxtime = 50;
opts.tol = 1e-8;
opts.tol_cg = 1e-3;

options.MaxIters = 100000;
options.StopTol = 1e-18;

pn = 50e-3;
opts.maxit = 100000;
n_sample = 50;

%% Run algorithms 
test = 1;   % training data id 
repeat = 1; % random initial point id  

tcinfo =  build_tcinfo_frSpT(T_data, opts.SR);
test_tcinfo{test} = tcinfo;  

%       
t0 = tic;
opts.U = initial_U_testtensor(tcinfo, opts);
opts.tinit =toc(t0);
initime{test}{repeat} = opts.tinit;
initial_u{test}{repeat} = opts.U;

if true
    % Model "GReg-TC" 
    opts.Lambda =  [0.131317553644499   0.131317553644499   0.131317553644499   0.119135840058818];   % 
    Out_CG{test}{repeat} = tc_altmin_cg(tcinfo, opts);

    opts.Lambda =[0.099765289117847   0.099765289117847   0.099765289117847  24.278091906112330];
    Out_ADMM{test}{repeat} = tc_altmin_ADMM(tcinfo, opts);
end

if true
    % Model "NuclReg-TC" 
    opts.Lambda =  [0.131317553644499   0.131317553644499   0.131317553644499   0];
    Out_CG1{test}{repeat} = tc_altmin_cg(tcinfo, opts);
    Out_ADMM1{test}{repeat} = tc_altmin_ADMM(tcinfo, opts);
end


