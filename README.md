# Alternating minimization algorithms for graph regularized tensor completion

This package implements the AltMin-CG and ADMM algorithms of [1] for tensor
completion with graph Laplacian-based regularization: 

(GReg-TC) S* = argmin [ loss(T(U)) + sum of lambda_i tr(Ui'*Li*Ui) ], 
            
where U=(U1, U2, U3) are the mi-by-R factor matrices of the CPD
model, and T(U) denotes the tensor product of CPD, and (L1,L2,L3) denotes
the graph Laplacian matrices used in the regularization term, along the
three dimensions of the tensor. 

Paper:

[1] Y. Guan, S. Dong, P.-A. Absil, and F. Glineur. Alternating minimization algorithms for graph-regularized tensor completion.  
arXiv preprint arXiv:2008.12876, pages 1--30, 2020.  
URL https://arxiv.org/pdf/2008.12876.pdf.

## Core functions  

- `core/tc_altmin_cg.m`     - Implementation of AltMin-CG algorithm
- `core/tc_altmin_ADMM.m`   - Implementation of ADMM algorithm

## Requirements

- Part of `Tensor Toolbox v2.5`  - For tensor formats and computations of the Khatri-Rao product and MTTKRP. See [`Tensor Toolbox`](https://www.tensortoolbox.org/) for more details. 


## Installation 
In the root path of this package, run `Compile.m`.

## Running demos

```
$ demo_movielens
$ demo_fia 
```

