The dataset is downloaded from
[here](http://www.models.life.ku.dk/Flow_Injection). 


1. tests on this dataset

see `./tests/test_fia.m`. 



### build similarity graph (in the "substance" dimension)
1. load data

```matlab
data = loaddata('fia'); 
```
2. build graph as described by [this paper,
Sec.4.1](https://ttic.uchicago.edu/~ryotat/papers/NarHayTomKas11.pdf):


```matlab
L = buildLmat_fia(data);
```
The function `./utils/buildLmat_fia.m` computes a 12x12 pairwise distance matrix
then computes the elementwise inverse. 


