function Mx = afun(x,Lap,R,m,C,index,kro)
% This function returns Ax = A*x, where A is the Hessian of the
% graph Laplacian-regularized least-squares problem (GRLS). 
 
% Compute B*vec(G'), returns a vector of size k*n as is s=vec(G'). 

X = reshape(x,[R,m]);
BVec = zeros(R,m);

for s = 1:m
    len = size(index{s},1);
    temp = zeros(R,1);
    if len ~= 0
        for s1 = 1:len
            temp = temp + kro(index{s}(s1),:)*X(:,s)*kro(index{s}(s1),:)';
        end
        BVec(:,s) = temp;
     end
 end
% Compute vectorization of X'*Lap.
LapG = X*Lap;  % 
CG = C*X;
Mx = BVec(:) + LapG(:) + CG(:);

end

         
