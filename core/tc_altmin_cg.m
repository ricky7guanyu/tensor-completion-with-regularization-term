function Out = tc_altmin_cg(tcinfo,opts)
%%
% This function implements the AltMin-CG algorithm of [1] for the tensor
% completion with graph Laplacian-based regularization: 
% 
% (GReg-TC) S* = argmin [ loss(T(U)) + sum of lambda_i tr(Ui'*Li*Ui) ], 
%             
% where U=(U1, U2, U3) are the mi-by-R factor matrices of the CPD
% model, and T(U) denotes the tensor product of CPD, and (L1,L2,L3) denotes
% the graph Laplacian matrices used in the regularization term, along the
% three dimensions of the tensor. 
% 
% Paper:
% 
% [1] Y. Guan, S. Dong, P.-A. Absil, and F. Glineur. Alternating minimization algorithms for graph-regularized tensor completion.  
% arXiv preprint arXiv:2008.12876, pages 1–30, 2020.  
% 
% URL https://arxiv.org/pdf/2008.12876.pdf.
% 
%%
%
% Input:
%       tcinfo:     observed entries of the underlying tensor
%       opts:       algorithm parameters
%

%% Parameters and initialization
if isfield(opts,'maxit'), maxit = opts.maxit; else maxit = 5000;  end; % max # of iterations
if isfield(opts,'R'), R = opts.R;   end;    % predefined rank
if isfield(opts,'m'),   m = opts.m;   end;  % dimension
if isfield(opts,'Lambda'),  Lambda = opts.Lambda;        end; % initial extrapolation weight
if isfield(opts,'L'), L = opts.L;   end ;% graph laplacian
if isfield(opts,'U'), U = opts.U;   end ;% initial
if ~isfield(opts,'maxit_cg'), opts.maxit_cg = 500; end;
if ~isfield(opts,'tol_cg'), opts.tol_cg = 1e-12; end;
if ~isfield(opts,'maxtime'), opts.maxtime = 10000;  end;
if ~isfield(opts,'tol'),   opts.tol = 1e-8;  end;% stopping tolerance
if ~isfield(opts,'tinit'),   tinit=0; else tinit = opts.tinit;  end;

k = length(m);
Z = sptensor([double(tcinfo.I),double(tcinfo.J),double(tcinfo.K)], tcinfo.Ttr, m);


Lap = {};
for i = 1:k
    Lap{i} = Lambda(i)*eye(m(i))+Lambda(end)*L{i};
end


fprintf(' iter\t RelErr val\t trainRMSE\t testRMSE\t  gradnorm\n');
%% Initialize the output
[Err, relErr, trRMSE, teRMSE] = compute_rmse(U, tcinfo);
Out = struct('train_RMSE', trRMSE, 'test_RMSE', teRMSE, 'Time', tinit, ...
    'iter', 0, 'U',{U}, 'relError', relErr, 'Error', Err);

tt = tinit;
for iter = 1:maxit
    mtt = {};
    temp3 = {};
    tstart = tic;
    perU = {};
    oldU = {};
    
    %% Check stopping criteria
    stop_reason = stopping_criteria(iter, Out, opts);
    if stop_reason.id
        Out.stop_reason = stop_reason;
        fprintf('>>>> %s\n', stop_reason.msg);
        break;
    end
    
    for i = 1:k
        if i == 1
            alpha1 = 2:k;
        elseif i == k
            alpha1 = 1:k-1;
        else
            alpha1 = [1:i-1,i+1:k];
        end
        
        oldU{i} = U{i};
        temp3{i} = zeros(R,R);
        % to store C^{i} which is a diagonal matrix
        for j = alpha1(1):alpha1(k-1)
            if j == alpha1(1)
                alpha2 = alpha1(2):alpha1(k-1);
            elseif j == alpha1(k-1)
                alpha2 = alpha1(1):alpha1(k-2);
            else
                alpha2 = [alpha1(1):j-1,j+1:alpha1(k-1)];
            end
            % Note: khatrirao(U1, U2) = U1 \odot U2.
            if k > 3
                Sigma = khatrirao(U{alpha2(2)},U{alpha2(1)});
                for order = 5:k
                    Sigma = khatrirao(U{alpha2(order-2)},Sigma);
                end
            elseif k == 3
                Sigma = U{alpha2(1)};
            end
            
            
            dig_Sigma = zeros(R,R);
            
            for t = 1:R
                dig_Sigma(t,t) = norm(Sigma(:,t),2)^2;
            end
            
            temp3{i} = temp3{i}+Lambda(j)*dig_Sigma;
        end
        temp1 = khatrirao(U{alpha1(k-1)},Sigma);
        % to generate the khatrirao product except U^{i}
        mtt{i} = mttkrp(Z,U,i); %this is Q^{i}
        vecB = reshape(mtt{i}',[m(i)*R,1]);
        perU1 = oldU{i}';
        s0 = perU1(:);
        
        % This is A^{i}
        A = @(s)afun_mex(s,Lap{i},R,m(i),temp3{i},uint32(tcinfo.Omega_mat{i}),temp1);
        
        % 
        [s,~] = pcg_2(A, vecB, opts.tol_cg, opts.maxit_cg,[],[], s0);
        
        perU{i} = reshape(s,[R,m(i)]);
        U{i} = perU{i}';
        grad_temp = norm((-vecB + afun_mex(s,Lap{i},R,m(i),temp3{i},uint32(tcinfo.Omega_mat{i}),temp1)), 2);
        Out.grad(i, iter) = grad_temp;
        Out.errorU(i, iter) = norm(U{i}-oldU{i},'fro');
    end
    tt = tt + toc(tstart);
    Out.Time(iter+1) = tt;

    %% Compute errors per iteration
    [Out.Error(iter+1), Out.relError(iter+1), ...
        Out.train_RMSE(iter+1), Out.test_RMSE(iter+1)] = compute_rmse(U, tcinfo);
    
    %% Print information per iteration
    gradnorm_ = sqrt(sum(Out.grad(:,iter).^2));
    fprintf('%5d\t%+.4e\t%+.4e\t%+.4e\t%.4e,\n', iter, Out.relError(iter+1),...
        Out.train_RMSE(iter+1), Out.test_RMSE(iter+1), gradnorm_);
end

Out.U = U;

end

