function U = initial_U_testtensor(tcinfo, opts)

m = opts.m; 
k = numel(m);
Z = sptensor([double(tcinfo.I),double(tcinfo.J),double(tcinfo.K)], tcinfo.Ttr, m);
R = opts.R; 

if ~isfield(opts,'method_init'), opts.method_init = 'random'; end;


switch opts.method_init

% Using High-Order SVD 
case 'HOSVD'
   Z1 = tenmat(Z,1);
   Z2 = tenmat(Z,2);
   Z3 = tenmat(Z,3);
   U = {};
  [U{1},~,~] = svds(Z1.data,R);
  [U{2},~,~] = svds(Z2.data,R);
  [U{3},~,~] = svds(Z3.data,R);

  lambda = [];
  
  for j = 1:R
      temp{1} = U{1}(:,j);
      temp{2} = U{2}(:,j);
      temp{3} = U{3}(:,j);
      temp_tensor = ktensor(temp);
      % Rank-1 component [U^{(1)}_{j}, U^{(2)}_{j}, U^{(3)}_{j}]. 
      lambda(j) = innerprod(Z, temp_tensor);
      if lambda(j) < 0
          lambda(j) = -lambda(j);
          U{1}(:,j) = -U{1}(:,j);
      end
  end
  
  for i = 1:3
      for j = 1:R
          U{i}(:,j) = lambda(j).^(1/3)*U{i}(:,j);
      end
  end

% Using random factor matrices  
case 'random'
    U = {};
    for i = 1:k
       U{i} = randn(m(i),R); 
    end
end

end
