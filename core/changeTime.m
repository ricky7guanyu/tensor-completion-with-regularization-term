function [A]=changeTime(A)
% Extract hour information for the time dimension

n=size(A,1);
for i=1:n
    date=datevec(A(i,4)/86400+datenum(1970,1,1));
    hour=date(4);
    if  hour>=0 && hour<5
        time=1;
    else if hour>=5 && hour<12
            time=2; 
         else if hour>=12 && hour <18
              time=3;   
             else
                 time=4;
             end
        end
    end
    A(i,4)=time;
end
