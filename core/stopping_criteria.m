function stop_reason = stopping_criteria(iter, Out, opts)

stop_reason.id = 0;
stop_reason.msg = 'ongoing'; 

% Running time limited by a time budget in opts. 
    if Out.Time(iter) > opts.maxtime 
        stop_reason.id = 1; 
        stop_reason.msg = 'Running time limit reached';
    end

% Relative error reached a tolerence threshold 
    if Out.relError(iter) <= opts.tol 
        stop_reason.id = 2; 
        stop_reason.msg = 'Relative error attained the tolerance value';
    end

end

