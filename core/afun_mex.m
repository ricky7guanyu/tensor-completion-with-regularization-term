function Mx = afun_mex(x,Lap,R,m,C, omega_two, kro)
% This function returns Ax = A*x, where A is the Hessian of the
% graph Laplacian-regularized least-squares problem (GRLS). 
% 
% This implementation uses the mex function taking the 
% following inputs:      
% - x:              the vectorized matrix variable
% - kro:            Hessian of the GRLS problem, 'A' 
% - omega_two:      2D indices, 'I' stored in omega_two(:,2) and 'J' in
%                     omega_two(:,1)
      
% Compute B*vec(G'), returns a vector of size k*n as is s=vec(G'). 
X = reshape(x,[R,m]);
xnew = comp_AOmegaVec(x, kro, omega_two(:,2), omega_two(:,1));

Cx = C*X;

% Compute vectorization of X'*Lap.
LapG = X*Lap;   
Mx = xnew + LapG(:) + Cx(:);
end

