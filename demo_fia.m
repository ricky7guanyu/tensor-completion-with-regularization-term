%% Demo on the FIA dataset 
clear all
startup

% Load the movielens data and generate similar matrices
data1 = load('u.data');
raw = loaddata('fia');
opts.m = [12,100,89];
%       Dataset in tensor format 
T_data = sptensor(reshape(raw.X,opts.m));
%       Sampling rate (among entries given in the dataset)
opts.SR = 0.05; % other values tested are between 0.01 and 0.1 


% Build graph information
Lmat_params = struct('methodname','fia');
graph = feed_Lmat(raw, Lmat_params);
opts.L{1} = graph.Lr;
%   Build a chain graph from prior knowledge on the third dimension
for i = 2:3
    Wsp = sparse(diag(ones(opts.m(i)-1,1),1))+sparse(diag(ones(opts.m(i)-1,1),-1));
    opts.L{i} = sparse(diag(sum(Wsp,2))) - Wsp;
end
 

% Algorithm's parameters
opts.R = 10;         % Maximal rank of the CPD model 

opts.maxtime = 50;
opts.tol = 1e-8;
opts.tol_cg = 1e-3;

options.MaxIters = 30000;
options.StopTol = 1e-18;

pn = 5e-2;
opts.maxit = 30000;
n_sample = 50;


%% Run algorithms 
test = 1;   % training data id 
repeat = 1; % random initial point id  

tcinfo =  build_tcinfo_frSpT(T_data, opts.SR);
test_tcinfo{test} = tcinfo;  

%       
t0 = tic;
opts.U = initial_U_testtensor(tcinfo, opts);
opts.tinit =toc(t0);
initime{test}{repeat} = opts.tinit;
initial_u{test}{repeat} = opts.U;

if true
    % Model "GReg-TC" 
    opts.Lambda = [0, 0, 0, 4e-3]; 
    Out_CG{test}{repeat} = tc_altmin_cg(tcinfo, opts);

    opts.Lambda = [1e-8, 1e-8, 1e-8, 4e-3];
    Out_ADMM{test}{repeat} = tc_altmin_ADMM(tcinfo, opts);
end


