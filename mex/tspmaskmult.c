/*=================================================================
% function T = tspmaskmult(U1, U2, U3, I, J, K)
% Computes [[U1,U2,U3]] at entries (I(s), J(s), K(s) and returns the result in
% a nnz-by-1 real double array T the same size as I (and J and K).
% I, J and K must be UINT32 arrays of size nnz-by-1.
%
% U1: n1-by-r, real, double
% U2: n2-by-r, real, double // attention (temp): B was of size r-by-n2
% U3: n3-by-r, real, double 
% I: nnz-by-1 row indices, uint32
% J: nnz-by-1 column indices, uint32
% K: nnz-by-1 3rd dimension indices, uint32
%
% Complexity: O(nnz*r)
%
% Warning: no check of data consistency is performed. Matlab will
% most likely crash if I, J or K go out of bounds.
%
% Compile with: mex tspmaskmult.c -largeArrayDims
%
% Note: This is an extension of spmaskmult.c to the case of 3-dimensional tensor. 
% September, 2019, Shuyu Dong, UCLouvain. 
 *=================================================================*/

/* #include <math.h> */
#include "mex.h"
#include "matrix.h"

/* Input Arguments */
#define	pU1	prhs[0]
#define	pU2	prhs[1]
#define	pU3	prhs[2]

#define	pI	prhs[3]
#define	pJ	prhs[4]
#define	pK	prhs[5]

/* Output Arguments */
#define	pT	plhs[0]

void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J, *K;
    double *U1, *U2, *U3, *T;
    mwSize n1, n2, n3, r, nnz;
    // mwIndex k, it;
    
    /* Check for proper number of arguments */
    if (nrhs != 6) { 
        mexErrMsgTxt("Six input arguments are required."); 
    } else if (nlhs != 1) {
        mexErrMsgTxt("A single output argument is required."); 
    } 
    
    /* Check argument classes */
    if(!mxIsDouble(pU1) || !mxIsDouble(pU2) || !mxIsDouble(pU3)) {
        mexErrMsgTxt("The tensor factors must be of class DOUBLE."); 
    }
    if(mxIsComplex(pU1) || mxIsComplex(pU2) || mxIsComplex(pU3)) {
        mexErrMsgTxt("The tensor factors must be REAL."); 
    }
    if(!mxIsUint32(pI) || !mxIsUint32(pJ) || !mxIsUint32(pK)) {
        mexErrMsgTxt("I, J and K must be of class UINT32."); 
    }
    
    /* Get and check the dimensions of input arguments */ 
    n1 = mxGetM(pU1);
    n2 = mxGetM(pU2);
    n3 = mxGetM(pU3);
    r = mxGetN(pU1);
    nnz = mxGetM(pI);
    if(mxGetN(pU2) != r || mxGetN(pU3) != r)
        mexErrMsgTxt("Matrix dimensions mismatch for U1, U2 and U3.");
    if(mxGetM(pJ) != nnz || mxGetN(pJ) != 1)
        mexErrMsgTxt("Matrix dimensions mismatch for I and J.");
    if(mxGetM(pK) != nnz || mxGetN(pK) != 1)
        mexErrMsgTxt("Matrix dimensions mismatch for I and K.");
     
    /* Get pointers to the data in A, B, I, J */
    U1 = mxGetPr(pU1);
    U2 = mxGetPr(pU2);
    U3 = mxGetPr(pU3);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    K = (uint32_T*) mxGetData(pK);
    
    /* Create a matrix for the ouput argument */ 
    pT = mxCreateDoubleMatrix(nnz, 1, mxREAL);
    if(pT == NULL)
        mexErrMsgTxt("TSPMASKMULT: Could not allocate X. Out of memory?");
    T = mxGetPr(pT);
    
    /* Compute */
    for(int it = 0; it < nnz; ++it)
    {
        /* 3-way inner product of the row I(it) of U1, the row J(it) of U2 and
         * the row K(it) of U3 */
        // T[it] = U1[I[it]-1] * U2[J[it]-1] * U3[K[it]-1] ;
        for(int k = 0; k < r; ++k)
        {
            T[it] += U1[I[it]-1 + n1*k] * U2[J[it]-1 + n2*k] * U3[K[it]-1 + n3*k];
        }
    }
    return;
}

